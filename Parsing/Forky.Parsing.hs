module Forky.Parsing 
     ( module Text.ParserCombinators.ReadP
     , module Forky.Parsing
     ) where

import Data.List
import Text.ParserCombinators.ReadP
import Control.Applicative hiding (optional, many)
import Data.Maybe

type Parser = ReadP

newtype Year = Year Int deriving (Show, Eq) 

yearField :: Int -> Int -> Parser Year
yearField minY maxY = Year <$> numRange minY maxY

numRange :: Int -> Int -> Parser Int
numRange minY maxY = do
  year <- number
  if year >= minY && year <= maxY
    then pure year
    else pfail

number :: (Integral a, Read a) => Parser a
number = read <$> many1 digit

digit :: Parser Char
digit = oneOf ['0'..'9']

alphanumeric :: Parser Char
alphanumeric = oneOf $ ['0'..'9'] ++ ['a'..'f']

oneOf :: [Char] -> Parser Char
oneOf str = satisfy (flip elem str)

exactly :: Int -> Parser a -> Parser [a]
exactly 0 p = pure []
exactly n p = (:) <$> p <*> exactly (n-1) p

anyChar :: ReadP Char
anyChar = satisfy (const True)

munchUntil :: Char -> Parser String
munchUntil c = munch (/= c) <* char ':'

-- Tokens
tok :: String -> Parser String
tok str = skipSpaces *> string str <* skipSpaces

parseMaybe :: Parser a -> String -> Maybe a
parseMaybe p str = do 
  x <- safeLast $ readP_to_S p str
  case x of
    (a,"") -> Just a
    _      -> Nothing

parseUnsafe :: Parser a -> String -> a
parseUnsafe p str = fromJust $ parseMaybe p str

safeLast :: [a] -> Maybe a
safeLast []     = Nothing
safeLast (x:xs) = Just (foldl' (\_ a -> a) x xs)