import Data.Maybe
import Data.List
import Debug.Trace 

-- Part1

part1 :: Integer -> [Maybe Integer] -> Integer
part1 time = uncurry (*)
           . minimum -- Orders by first element of tuple first, then second
           . map (\n -> (n - time `mod` n, n))
           . catMaybes 

-- Part 2

-- Only works if first element is Just, slow as hell
part2 :: [Maybe Integer] -> Maybe Integer
part2 buses = find (foo buses)
            $ [0, firstTime..]
  where
    Just firstTime = head buses

foo :: [Maybe Integer] -> Integer -> Bool
foo buses t = and
            . zipWith arrivesAtTime [t..]
            $ buses

arrivesAtTime :: Integer -> Maybe Integer -> Bool
arrivesAtTime t Nothing  = True 
arrivesAtTime t (Just n) = t `mod` n == 0

part2CRT :: [Maybe Integer] -> Integer
part2CRT = chineseRemSeive
         . map (\(a, Just n) -> (a `mod` n, n))
         . filter (isJust . snd)
         . zip [0,-1..]

-- fst integer = lhs of mod
-- snd integer = rhs of mod
chineseRemSeive :: [(Integer, Integer)] -> Integer
chineseRemSeive = fst
                . foldl' f k
                . sortOn snd
  where
    k = (0, [1]) 

    f :: (Integer, [Integer]) -> (Integer, Integer) -> (Integer, [Integer])
    f (x, ns) (a,n') = (\x' -> (x', n' : ns))
                     . fromJust
                     . find (\x' -> x' `rem` n' == a)
                     $ [x,x + product ns..]

-- Main

main :: IO ()
main = do
  (arrival, times) <- getInput
  print $ part1 arrival times
  print $ part2CRT times

getInput :: IO (Integer, [Maybe Integer])
getInput = getInputFrom "input"

getInputFrom :: FilePath -> IO (Integer, [Maybe Integer])
getInputFrom filePath = do
  [arrivalString, timesString] <- lines <$> readFile filePath
  let arrival = read arrivalString :: Integer
      times   = map (\c -> if c == "x" then Nothing else Just (read c))
              $ split (== ',') timesString
  pure (arrival, times)


-- From the extra package, Data.List.Extra

-- | Splits a list into components delimited by separators,
-- where the predicate returns True for a separator element.  The
-- resulting components do not contain the separators.  Two adjacent
-- separators result in an empty component in the output.
--
-- > split (== 'a') "aabbaca" == ["","","bb","c",""]
-- > split (== 'a') ""        == [""]
-- > split (== ':') "::xyz:abc::123::" == ["","","xyz","abc","","123","",""]
-- > split (== ',') "my,list,here" == ["my","list","here"]
split :: (a -> Bool) -> [a] -> [[a]]
split f [] = [[]]
split f (x:xs) | f x = [] : split f xs
split f (x:xs) | y:ys <- split f xs = (x:y) : ys