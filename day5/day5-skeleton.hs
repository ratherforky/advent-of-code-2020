part1 :: [String] -> Int
part1 = error "Part 1 incomplete"

part2 :: [String] -> Int
part2 = error "Part 2 incomplete"

main :: IO ()
main = do
  contents <- readFile "input"
  let contentLines = lines contents
  print (part1 contentLines)
  print (part2 contentLines)