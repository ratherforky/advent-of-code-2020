import Data.List

part1 :: [String] -> Int
part1 = maximum . map calcSeatID

calcSeatID :: String -> Int
calcSeatID str = row * 8 + col
  where
    row = sum $ zipWith (*) (map toBinBF $ take 7 str) [ 2 ^ x | x <- [6,5..0]]
    col = sum $ zipWith (*) (map toBinLR $ drop 7 str) [ 2 ^ x | x <- [2,1,0]]

    toBinBF :: Char -> Int
    toBinBF 'F' = 0
    toBinBF 'B' = 1
    toBinBF  _  = error "Must be B or F"

    toBinLR :: Char -> Int
    toBinLR 'L' = 0
    toBinLR 'R' = 1
    toBinLR  _  = error "Must be L or R"

part2 :: [String] -> Int
part2 = findHole . sort . map calcSeatID

findHole :: [Int] -> Int
findHole [] = error "Couldn't find hole, empty list"
findHole [x] = error "Couldn't find hole, singleton list"
findHole (x:y:xs)
  | x + 1 == y = findHole (y:xs)
  | otherwise  = x+1

main :: IO ()
main = do
  contents <- readFile "input"
  let contentLines = lines contents
  print (part1 contentLines)
  print (part2 contentLines)