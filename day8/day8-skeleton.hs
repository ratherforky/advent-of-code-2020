import Forky.Parsing
import Control.Applicative

-- Types

data Instr = Instr Opcode Operand deriving (Show, Eq)
data Opcode = NOP | ACC | JMP deriving (Show, Eq, Enum)
type Operand = Int

-- Part 1

part1 :: [Instr] -> Int
part1 = error "Implement part 1"

-- Part 2

part2 :: [Instr] -> [Int]
part2 = error "Implement part 2"

main :: IO ()
main = do
  contents <- readFile "input"
  let instrs = map (parseUnsafe instr) $ lines contents
  print (part1 instrs)
  print (part2 instrs)

-- Parsing

instr :: Parser Instr
instr = Instr <$> opcode <*> operand

opcode :: Parser Opcode
opcode = NOP <$ tok "nop"
     <|> ACC <$ tok "acc"
     <|> JMP <$ tok "jmp"

operand :: Parser Operand
operand = tok "+" *> number
      <|> tok "-" *> number <**> pure (0 -)
