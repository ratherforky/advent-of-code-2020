{-# language MultiWayIf #-}

import Forky.Parsing hiding (get)
import Forky.Zippy
import Control.Applicative hiding (ZipList)
import Control.Monad.Extra
import Data.IntSet (IntSet)
import qualified Data.IntSet as S
import Control.Monad.Trans.State.Strict

-- Parsing and types

data Instr = Instr Opcode Operand deriving (Show, Eq)
data Opcode = NOP | ACC | JMP deriving (Show, Eq, Enum)
type Operand = Int

instr :: Parser Instr
instr = Instr <$> opcode <*> operand

opcode :: Parser Opcode
opcode = NOP <$ tok "nop"
     <|> ACC <$ tok "acc"
     <|> JMP <$ tok "jmp"

operand :: Parser Operand
operand = tok "+" *> number
      <|> tok "-" *> number <**> pure (0 -)

-- Stateful performing of instructions

type CompState = State (ZipList (Int, Instr), IntSet, Int)

data Status = FoundLoop | Continuing | Completed deriving (Eq, Show)

performInstrs' :: CompState Status
performInstrs' = do
  s <- performStep'
  case s of
    Continuing -> performInstrs'
    _          -> pure s

performStep' :: CompState Status
performStep' = do
  instrs <- getInstrs
  let (i, instr) = getZ instrs
  setI <- getPrevInstrIndexs
  if | S.member i setI  -> pure FoundLoop
     | atEnd instrs -> Completed <$ performInstr instr
     | otherwise    -> addToSeenInstrs i 
                         >> Continuing <$ performInstr instr


addToSeenInstrs :: Int -> CompState ()
addToSeenInstrs i = modify (\(zl, s, acc) -> (zl, S.insert i s, acc))

performInstr :: Instr -> CompState ()
performInstr (Instr NOP _) = modify (\(zl, s, acc) -> (after zl, s, acc))
performInstr (Instr ACC x) = modify (\(zl, s, acc) -> (after zl, s, acc + x))
performInstr (Instr JMP x) 
  = modify (\(zl, s, acc)
                -> if x >= 0
                    then (right x zl, s, acc)
                    else (left (abs x) zl, s, acc))

getInstrs :: CompState (ZipList (Int, Instr))
getInstrs = gets (\(x, _, _) -> x)

getPrevInstrIndexs :: CompState (IntSet)
getPrevInstrIndexs = gets (\(_, y, _) -> y)

-- Part 1

part1 :: ZipList (Int, Instr) -> Int
part1 zl = (\(x,y,z) -> z) $ execState performInstrs' (zl, S.empty, 0)

-- Part 2

part2 :: ZipList (Int, Instr) -> [Int]
part2 zl = map (\(_,(_,_,acc)) -> acc)
         . filter (\(x, s) -> x == Completed) 
         . map ((\zl -> runState performInstrs' (zl, S.empty, 0)) . firstZ)
         $ gen zl [] 
  where
    -- Generate all the instruction ZipLists that have only one instruction changed
    gen :: ZipList (Int, Instr) -> [ZipList (Int, Instr)] -> [ZipList (Int, Instr)]
    gen zl@(ZL xs (i, Instr NOP oprnd) []) acc = set (i, Instr JMP oprnd) zl : acc
    gen zl@(ZL xs (i, Instr JMP oprnd) []) acc = set (i, Instr NOP oprnd) zl : acc
    gen (ZL _ _ []) acc = acc
    gen zl acc = case getZ zl of
      (i, Instr NOP oprnd) -> gen (after zl) (set (i, Instr JMP oprnd) zl : acc)
      (i, Instr JMP oprnd) -> gen (after zl) (set (i, Instr NOP oprnd) zl : acc)
      x                    -> gen (after zl) acc

main :: IO ()
main = do
  contents <- readFile "input"
  let instrs = fromList $ zip [1..] $ map (parseUnsafe instr) $ lines contents
  print (part1 instrs)
  print (part2 instrs)