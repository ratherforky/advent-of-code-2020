import Data.List
import qualified Data.BloomFilter.Easy as B

find2020 :: [Int] -> Int
find2020 xs = head [ x * y | x <- xs, y <- xs, x + y == 2020]

find2020Three :: [Int] -> Int
find2020Three ys = head [ x * y * z | x <- xs, y <- xs, z <- xs, x + y + z == 2020]
  where
    xs = sort ys

choose :: Int -> [a] -> [[a]]
choose 0 _ = [[]]
choose _ [] = []
choose k (x:xs) = map (x:) (choose (k - 1) xs) ++ choose k xs

-- Trying to gen combos of items earlier in the list first
choose2 :: [a] -> [[a]]
choose2 xs = go True xs
  where
    go :: Bool -> [a] -> [[a]]
    go _ [] = []
    go _ [_] = []
    go True (x:y:xs) = [x,y] : go False (x:xs) ++ go False (y:xs)
    go False (x:y:xs) = [x,y] : go False (y:xs) ++ go False (x:xs)

find2020Robust :: [Int] -> Int
find2020Robust = product 
               . head
               . filter ((2020 ==) . sum)
               . choose 3
               . sort

-- O(n^2)
find2020n2Bloom :: [Int] -> [Int]
find2020n2Bloom xs
  = map (\(sumOfNs, ns) -> product ns * (2020 - sumOfNs))
  . filter (\(sumOfNs, _) -> B.elem (2020 - sumOfNs) bfilter)
  . map (\ns -> (sum ns, ns)) 
  . choose 2
  $ sortedList
  where
    sortedList = sort xs
    bfilter = B.easyList 0.0001 sortedList

main :: IO ()
main = do
  contents <- readFile "inputBig"
  let intLines = map read (lines contents) :: [Int]
  print (find2020 intLines)
  print (find2020n2Bloom intLines)
  print (find2020Three intLines)
  print (find2020Robust intLines)
  