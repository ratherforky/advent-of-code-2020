find2020 :: [Int] -> Int
find2020 xs = error "Part 1"

find2020Three :: [Int] -> Int
find2020Three xs = error "Part 2"

main :: IO ()
main = do
  contents <- readFile "input"
  let intLines = map read (lines contents) :: [Int]
  print (find2020 intLines)
  print (find2020Three intLines)