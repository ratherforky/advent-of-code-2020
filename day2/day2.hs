import Data.Foldable
import Data.Bits (xor)

part1 :: [(Int, Int, Char, String)] -> Int
part1 = length . filter isValid

isValid :: (Int, Int, Char, String) -> Bool
isValid (minFreq, maxFreq, char, password)
  = minFreq <= charFreq && maxFreq >= charFreq
  where
    charFreq = length $ filter (== char) password

part2 :: [(Int, Int, Char, String)] -> Int
part2 = length . filter isValid2

isValid2 :: (Int, Int, Char, String) -> Bool
isValid2 (i, j, char, password)
  = xor (atIndex char i password) 
        (atIndex char j password)


atIndex :: Char -> Int -> String -> Bool
atIndex c i str = case str !? (i-1) of
  Just c' -> c == c'
  Nothing -> False

-- Adapted from (!!) in Data.List
(!?) :: [a] -> Int ->  Maybe a
{-# INLINABLE (!?) #-}
xs !? n
  | n < 0     = Nothing
  | otherwise = foldr (\x r k -> case k of
                                   0 -> Just x
                                   _ -> r (k-1)) (const Nothing) xs n

main :: IO ()
main = do
  contents <- readFile "input"
  let contentLines = lines contents :: [String]
  let parsedLines = map parse contentLines
  print (part1 parsedLines)
  print (part2 parsedLines)

parse :: String -> (Int, Int, Char, String)
parse str = (read minFreq, read maxFreq, char, password )
  where
    [minFreq, maxFreq, [char], _, password] = split (\c -> elem c "- :") str


-- From the extra package, Data.List.Extra

-- | Splits a list into components delimited by separators,
-- where the predicate returns True for a separator element.  The
-- resulting components do not contain the separators.  Two adjacent
-- separators result in an empty component in the output.
--
-- > split (== 'a') "aabbaca" == ["","","bb","c",""]
-- > split (== 'a') ""        == [""]
-- > split (== ':') "::xyz:abc::123::" == ["","","xyz","abc","","123","",""]
-- > split (== ',') "my,list,here" == ["my","list","here"]
split :: (a -> Bool) -> [a] -> [[a]]
split f [] = [[]]
split f (x:xs) | f x = [] : split f xs
split f (x:xs) | y:ys <- split f xs = (x:y) : ys