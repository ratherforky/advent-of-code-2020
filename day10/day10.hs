import Data.List


part1 :: [Int] -> Int
part1 = (\(_, d1, d3) -> d1 * (d3 + 1)) . foldr f (0,0,0) . sortBy (flip compare)
  where
    f :: Int -> (Int, Int, Int) -> (Int, Int, Int)
    f x (prev, diff1, diff3)
      | diff == 1 = (x, diff1 + 1, diff3)
      | diff == 3 = (x, diff1, diff3 + 1)
      | otherwise = (x, diff1, diff3)
      where
        diff = x - prev

part2 :: [Int] -> Int
part2 xs = snd
         . head -- should be count tuple for device joltage 
         . foldl' f [(0,1)] 
         $ sorted <> [deviceJoltage]
  where
    f :: [(Int, Int)] -> Int -> [(Int, Int)]
    f acc x = addCountForNewVal x
            . filter (within3 x . fst)
            $ acc

    sorted = sort xs
    deviceJoltage = last sorted + 3

within3 :: Int -> Int -> Bool
within3 x y = diff <= 3 && diff >= -3
  where diff = y - x

-- Sum up the previous counts,
-- add a new value with that count
addCountForNewVal :: Int -> [(Int, Int)] -> [(Int, Int)]
addCountForNewVal x xs = (x, sum $ map snd xs) : xs

main :: IO ()
main = do
  contents <- readFile "input"
  let intLines = map read $ lines contents :: [Int]
      part1Ans = part1 intLines
  print (part1 intLines)
  print (part2 intLines)

getSimpleInput :: IO [Int]
getSimpleInput = (map read . lines) <$> readFile "input-short"

getMediumInput :: IO [Int]
getMediumInput = (map read . lines) <$> readFile "input-medium"
