part1 :: [Int] -> Int
part1 = error "Implement part 1"

part2 :: [Int] -> Int
part2 ints = error "Implement part 2"

main :: IO ()
main = do
  contents <- readFile "input"
  let intLines = map read $ lines contents :: [Int]
      part1Ans = part1 intLines
  print (part1 intLines)
  print (part2 intLines)