import Data.List

part1 :: [[String]] -> Int
part1 = sum . map (length . nub) . map concat

part2 :: [[String]] -> Int
part2 xss = sum $ map foo xss
  where
    foo :: [String] -> Int
    foo strs = length (filter (length strs ==) $ map length $ group (sort (concat strs)))

main :: IO ()
main = do
  contents <- readFile "input"
  let forms = groupAnswers contents
  print (part1 forms)
  print (part2 forms)

groupAnswers :: String -> [[String]]
groupAnswers = filter (not . elem "") . groupBy p . lines
  where
    p :: String -> String -> Bool
    p "" ""  = True
    p "" str = False
    p str "" = False
    p _   _  = True