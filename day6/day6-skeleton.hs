import Data.List

part1 :: [[String]] -> Int
part1 = error "Part 2 incomplete"

part2 :: [[String]] -> Int
part2 = error "Part 2 incomplete"

main :: IO ()
main = do
  contents <- readFile "input"
  let forms = groupAnswers contents
  print (part1 forms)
  print (part2 forms)

groupAnswers :: String -> [[String]]
groupAnswers = filter (not . elem "") . groupBy p . lines
  where
    p :: String -> String -> Bool
    p "" ""  = True
    p "" str = False
    p str "" = False
    p _   _  = True
