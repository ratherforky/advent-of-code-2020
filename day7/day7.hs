import Forky.Parsing -- In Parsing/Forky.Parsing.hs
import Data.Maybe
import Data.Set (Set)
import qualified Data.Set as S
import Data.Map.Lazy (Map)
import qualified Data.Map.Lazy as M

newtype Colour = C String deriving (Show, Eq, Ord)

data BagRule = BR Colour [(Int, Colour)] deriving (Show, Eq, Ord)

part1 :: [BagRule] -> Int
part1 rules = sub 1 $ length $ converge findNewBags (S.singleton (C "shiny gold"))--undefined
  where
    findNewBags :: Set Colour -> Set Colour
    findNewBags colours = foldr f colours rules
      where
        f :: BagRule -> Set Colour -> Set Colour
        f (BR c cs) acc
          | or (map (flip S.member acc . snd) cs) = S.insert c acc
          | otherwise = acc

    sub :: Int -> Int -> Int
    sub y x = x - y

converge :: Eq a => (a -> a) -> a -> a
converge f k = go $ iterate f k
  where
    go (x:y:ys)
      | x == y    = x
      | otherwise = go (y:ys)

-- Part 2

part2 :: [BagRule] -> Int
part2 brs = numBags (C "shiny gold")
  where
    m = M.fromList $ map (\(BR c cs) -> (c,cs)) brs

    -- m M.! (C "shiny gold")

    numBags :: Colour -> Int
    numBags c = sum 
              . map (\(n, c) -> n + n * numBags c)
              $ m M.! c -- Get bags needed inside this colour of bag

-- Parsing

parseRule :: String -> BagRule
parseRule = parseUnsafe rule

rule :: Parser BagRule
rule = BR <$> colour
          <*  tok "contain"
          <*> (many rhs <|> [] <$ tok "no other bags.")

colour :: Parser Colour
colour = C <$> manyTill anyChar (tok "bag" *> optional (tok "s"))

rhs :: Parser (Int, Colour)
rhs = (,) <$> number <* skipSpaces
          <*> colour 
          <*  optional (oneOf ".,")
          <*  skipSpaces

-- main

main :: IO ()
main = do
  contents <- readFile "input"
  let rules = map parseRule $ lines contents
  print (part1 rules)
  print (part2 rules)
