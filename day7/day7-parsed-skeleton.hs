import Forky.Parsing

type Colour = String

data BagRule = BR Colour [(Int, Colour)] deriving (Show, Eq, Ord)

part1 :: [BagRule] -> Int
part1 = undefined

-- Part 2

part2 :: [BagRule] -> Int
part2 = undefined

-- main

main :: IO ()
main = do
  contents <- readFile "input"
  let rules = map parseRule $ lines contents
  print (part1 rules)
  print (part2 rules)

-- Parsing

parseRule :: String -> BagRule
parseRule = parseUnsafe rule

rule :: Parser BagRule
rule = BR <$> colour
          <*  tok "contain"
          <*> (many rhs <|> [] <$ tok "no other bags.")

colour :: Parser Colour
colour = manyTill anyChar (tok "bag" *> optional (tok "s"))

rhs :: Parser (Int, Colour)
rhs = (,) <$> number <* skipSpaces
          <*> colour 
          <*  optional (oneOf ".,")
          <*  skipSpaces