import Data.Word
import Forky.Parsing
import Control.Applicative hiding (many)
import Util.Bits
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import Data.List


data Instr = Mask [Maybe Bool] | Mem Word64 Word64 deriving (Eq, Show)

part1 :: [Instr] -> Word64
part1 = sum . snd . foldl' f ([], M.empty)
  where
    f :: ([Maybe Bool], Map Word64 Word64) -> Instr -> ([Maybe Bool], Map Word64 Word64)
    f (_, mww) (Mask m)  = (m, mww)
    f (m, mww) (Mem i x) = (m, M.insert i (applyMask m x) mww)


maskTest :: [Maybe Bool]
maskTest = parseUnsafe mask "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X"

applyMask :: [Maybe Bool] -> Word64 -> Word64
applyMask m i = fromListLE
              . zipWith foo (reverse m)
              $ toListLE i
  where
    foo :: Maybe Bool -> Bool -> Bool
    foo (Just x) _ = x
    foo Nothing  y = y

part2 :: [Instr] -> Word64
part2 = sum . snd . foldl' f ([], M.empty)
  where
    f :: ([Maybe Bool], Map Word64 Word64) -> Instr -> ([Maybe Bool], Map Word64 Word64)
    f (_, mww) (Mask m)  = (m, mww)
    f (m, mww) (Mem i x) = (m, mww')
      where
        mww' = foldl' (\mww'' i -> M.insert i x mww'') mww (applyMask2 m i)

maskTest2 :: [Maybe Bool]
maskTest2 = parseUnsafe mask "00000000000000000000000000000000X0XX"


applyMask2 :: [Maybe Bool] -> Word64 -> [Word64]
applyMask2 m i = map fromListLE
               . allFloatingVals
               . zipWith foo (reverse m)
               $ toListLE i
  where
    foo :: Maybe Bool -> Bool -> Maybe Bool
    foo (Just False) y = Just y
    foo (Just True ) _ = Just True
    foo Nothing      y = Nothing

allFloatingVals :: [Maybe Bool] -> [[Bool]]
allFloatingVals [] = [[]]
allFloatingVals (Just b  : xs) = map (b :) $ allFloatingVals xs
allFloatingVals (Nothing : xs) = map (False :) r ++ map (True :) r
  where
    r = allFloatingVals xs 

main :: IO ()
main = do
  instrs <- getInputFrom "input"
  print $ part1 instrs
  print $ part2 instrs


getInputFrom :: FilePath -> IO [Instr]
getInputFrom filePath = map (parseUnsafe instr) . lines <$> readFile filePath

instr :: Parser Instr
instr = Mask <$ tok "mask =" <*> mask
    <|> Mem  <$ tok "mem[" <*> number <* tok "] =" <*> number

mask :: Parser [Maybe Bool]
mask = many $ choice [ Nothing <$ char 'X'
                     , Just <$> (False <$ char '0' <|> True <$ char '1')
                     ]

