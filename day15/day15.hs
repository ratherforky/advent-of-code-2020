import Data.List
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as M

input :: [Int]
input = [16,12,1,0,15,7,11]

part1 :: [Int] -> Int
part1 xs = playNumGame xs !! 2019

type State = (Int, Int, HashMap Int Int)

playNumGame :: [Int] -> [Int]
playNumGame startingNums = startingNums ++ unfoldr f k
  where
    k :: State
    k = ( last startingNums                   -- Last number seen
        , length startingNums + 1             -- Turn number
        , M.fromList $ zip startingNums [1..] -- Map from the number to the last time it was said
        )

    f :: State -> Maybe (Int, State)
    f (lastNum, turn, mii) = case M.lookup lastNum mii of
      Nothing       -> Just (0, (0, turn+1, M.insert lastNum (turn - 1) mii))
      Just prevTurn -> Just (lastNum', (lastNum', turn +1, M.insert lastNum (turn - 1) mii))
        where
          lastNum' = turn - 1 - prevTurn


part2 :: [Int] -> Int
part2 = playNumGameN 30000000

-- Not super fast, but it gets there in a reasonable amount of time
-- playNumGame does it in a similar amount of time too though
playNumGameN :: Int -> [Int] -> Int
playNumGameN n startingNums = go n k
  where
    go :: Int -> State -> Int
    go n (lastNum, turn, mii)
      | n == turn - 1 = lastNum
      | otherwise = case M.lookup lastNum mii of
        Nothing       -> go n (0, turn+1, M.insert lastNum (turn - 1) mii)
        Just prevTurn -> go n (lastNum', turn +1, M.insert lastNum (turn - 1) mii)
          where
            lastNum' = turn - 1 - prevTurn
    
    k :: State
    k = ( last startingNums                   -- Last number seen
        , length startingNums + 1             -- Turn number
        , M.fromList $ zip startingNums [1..] -- Map from the number to the last time it was said
        )


main :: IO ()
main = do
  print $ part1 input
  print $ part2 input
  print $ playNumGame input !! 29999999