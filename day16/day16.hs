{-# language DeriveGeneric, DeriveAnyClass #-}
import Forky.Parsing
import Data.List
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as M
import GHC.Generics
import Data.Hashable

data Rule = Rule String Range Range deriving (Eq, Show, Generic, Hashable)

type Range = (Int, Int)

-- Part 1

part1 :: [Rule] -> [[Int]] -> Int
part1 rs nearbyTickets
  = sum
  . filter (invalidBy rs)
  $ concat nearbyTickets

invalidBy :: [Rule] -> Int -> Bool
invalidBy rs x = not 
               . any (inRange x)
               $ allRanges
  where
    allRanges :: [Range]
    allRanges = concatMap getRanges rs

getRanges :: Rule -> [Range]
getRanges (Rule _ r1 r2) = [r1,r2]


inRange :: Int -> Range -> Bool
inRange x (minV, maxV) = x >= minV && x <= maxV

-- Part 2

part2 :: [Rule] -> [Int] -> [[Int]] -> Int
part2 rs myTicket nearbyTickets
  = foldr (\i acc -> (myTicket !! i) * acc) 1
  . map fst                        -- [Int], indexes of the columns with departure in the name
  . filter (\(_, Rule field _ _) -> "departure" `isPrefixOf` field)
  . matchRulesToColumns            -- [(Int,Rule)], Rules with an assigned column
  . allPossibleColumnsForRules rs  -- HashMap Rule [Int], rules with all columns they could match to
  . zip [0..]                      -- [(Int, [Int])], columns with index
  . transpose                      -- [[Int]], columns
  . (myTicket :)                   -- [[Int]], rows
  $ removeInvalid rs nearbyTickets -- [[Int]], rows

allPossibleColumnsForRules :: [Rule] -> [(Int, [Int])] -> HashMap Rule [Int]
allPossibleColumnsForRules rs = foldr f M.empty
  where
    f :: (Int, [Int]) -> HashMap Rule [Int] -> HashMap Rule [Int]
    f (i, col) hmap
      = foldr (\r m -> M.insertWith (++) r [i] m) hmap
      $ filter (\r -> all (satisfiesRule r) col) rs

matchRulesToColumns :: HashMap Rule [Int] -> [(Int,Rule)]
matchRulesToColumns = foldl' f []
                    . sortOn (\(_,xs) -> length xs)
                    . M.toList
  where
    f :: [(Int,Rule)] -> (Rule, [Int]) -> [(Int,Rule)]
    f xs (r, possibleIndexes)
      = case possibleIndexes \\ (map fst xs) of
          [i] -> (i, r) : xs
          _   -> error "Hopefully doesn't happen?"

satisfiesRule :: Rule -> Int -> Bool
satisfiesRule (Rule _ r1 r2) x = inRange x r1 || inRange x r2

removeInvalid :: [Rule] -> [[Int]] -> [[Int]]
removeInvalid rs = filter (not . any (invalidBy rs))

-- Main

main :: IO ()
main = do
  (rs, myTicket, nearbyTickets) <- getInputFrom "input"
  print $ part1 rs nearbyTickets
  print $ part2 rs myTicket nearbyTickets

getInputFrom :: String -> IO ([Rule], [Int], [[Int]])
getInputFrom fileName = parseUnsafe wholeFile <$> readFile fileName


-- Parsing


rules :: Parser [Rule]
-- rules = many (rule <* skipSpaces) -- Very slow for some reason
rules = sepBy1 rule (char '\n') 

rule :: Parser Rule
rule = Rule <$> munchUntil ':' <* skipSpaces 
            <*> range 
            <*  tok "or"
            <*> range

range :: Parser (Int, Int)
range = (,) <$> number <* char '-' <*> number

ticket :: Parser [Int]
ticket = sepBy1 number (char ',')

tickets :: Parser [[Int]]
tickets = sepBy1 ticket (char '\n') 

wholeFile :: Parser ([Rule], [Int], [[Int]])
wholeFile = (,,) <$> rules
                 <*  tok "your ticket:"
                 <*> ticket
                 <*  tok "nearby tickets:"
                 <*> tickets
                 <*  skipSpaces


-- Testing

rule1 = Rule "departure date" (37,145) (168,965)
