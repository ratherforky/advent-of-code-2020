{-# language MultiWayIf #-}

import Data.List
import Data.Maybe
import Text.ParserCombinators.ReadP
import Control.Applicative hiding (optional)

-- Part 1

part1 :: [String] -> Int
part1 = error "Implement part 1"

-- Part 2 

part2 :: [String] -> Int
part2 = error "Implement part 2"

-- Main

main :: IO ()
main = do
  contents <- readFile "input"
  let pps = separatePassports contents
  print (part1 pps)
  print (part2 pps)

separatePassports :: String -> [String]
separatePassports = foldr f [] . lines
  where
    f :: String -> [String] -> [String]
    f str' [] = [str']
    f str' ("":strs)  = str':strs
    f ""   (str:strs) = "":str:strs
    f str' (str:strs) = (str' ++ " " ++ str) : strs 
