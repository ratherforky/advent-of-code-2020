import Data.List
import Data.Maybe
import Text.ParserCombinators.ReadP
import Control.Applicative hiding (optional)

-- Part 1

part1 :: [String] -> Int
part1 = length . filter isValid
  where
    isValid :: String -> Bool
    isValid str = and $ map (\field -> isInfixOf field str) fields

    fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

-- Part 2 

data Passport = PP
  { _byr :: Year       -- four digits; at least 1920 and at most 2002
  , _ecl :: EyeColour  -- exactly one of: amb blu brn gry grn hzl oth
  , _eyr :: Year       -- four digits; at least 2020 and at most 2030
  , _hcl :: RGB        -- a # followed by exactly six characters 0-9 or a-f
  , _hgt :: Height     -- a number followed by either cm or in
  , _iyr :: Year       -- four digits; at least 2010 and at most 2020
  , _pid :: PassportID -- a nine-digit number, including leading zeroes
  } deriving (Eq, Show)

newtype Year = Year Int deriving (Show, Eq) 
data Height = CM Int | IN Int deriving (Show, Eq) 
data RGB = RBG Char Char Char Char Char Char deriving (Show, Eq) 
data EyeColour = Amber | Blue | Brown | Grey | Green | Hazel | Other deriving (Show, Eq) 
newtype PassportID = PPID String deriving (Show, Eq) 

part2 :: [String] -> Int
part2 = length . catMaybes . map (parseMaybe passport . sortFields)

sortFields :: String -> String
sortFields = unwords . sort . words

type Parser = ReadP

passport :: Parser Passport
passport = PP 
       <$ tok "byr:" <*> byr
       <* optional cid
       <* tok "ecl:" <*> ecl
       <* tok "eyr:" <*> eyr
       <* tok "hcl:" <*> hcl
       <* tok "hgt:" <*> hgt
       <* tok "iyr:" <*> iyr
       <* tok "pid:" <*> pid

byr :: Parser Year
byr = yearField 1920 2002

cid :: Parser ()
cid = tok "cid:" *> munch (/= ' ') *> pure ()

ecl :: Parser EyeColour
ecl = Amber <$ tok "amb"
  <|> Blue  <$ tok "blu"
  <|> Brown <$ tok "brn"
  <|> Grey  <$ tok "gry"
  <|> Green <$ tok "grn"
  <|> Hazel <$ tok "hzl"
  <|> Other <$ tok "oth"

eyr :: Parser Year
eyr = yearField 2020 2030

hcl :: Parser RGB
hcl = RBG <$ skipSpaces 
         <*  char '#'
         <*> alphanumeric
         <*> alphanumeric
         <*> alphanumeric
         <*> alphanumeric
         <*> alphanumeric
         <*> alphanumeric

hgt :: Parser Height
hgt = CM <$> numRange 150 193 <* tok "cm"
  <|> IN <$> numRange 59  76  <* tok "in"

iyr :: Parser Year
iyr = yearField 2010 2020

pid :: Parser PassportID
pid = PPID <$> exactly 9 digit

-- Useful parser combinators

yearField :: Int -> Int -> Parser Year
yearField minY maxY = Year <$> numRange minY maxY

numRange :: Int -> Int -> Parser Int
numRange minY maxY = do
  year <- number
  if year >= minY && year <= maxY
    then pure year
    else pfail

number :: Parser Int
number = read <$> many1 digit

digit :: Parser Char
digit = oneOf ['0'..'9']

alphanumeric :: Parser Char
alphanumeric = oneOf $ ['0'..'9'] ++ ['a'..'f']

oneOf :: [Char] -> Parser Char
oneOf str = satisfy (flip elem str)

exactly :: Int -> Parser a -> Parser [a]
exactly 0 p = pure []
exactly n p = (:) <$> p <*> exactly (n-1) p

-- Tokens
tok :: String -> Parser String
tok str = skipSpaces *> string str <* skipSpaces

parseMaybe :: Parser a -> String -> Maybe a
parseMaybe p str = do 
  x <- safeLast $ readP_to_S p str
  case x of
    (a,"") -> Just a
    _      -> Nothing

safeLast :: [a] -> Maybe a
safeLast []     = Nothing
safeLast (x:xs) = Just (foldl' (\_ a -> a) x xs)

-- main

main :: IO ()
main = do
  contents <- readFile "input"
  let pps = separatePassports contents
  print (part1 pps)
  print (part2 pps)

separatePassports :: String -> [String]
separatePassports = foldr f [] . lines
  where
    f :: String -> [String] -> [String]
    f str' [] = [str']
    f str' ("":strs)  = str':strs
    f ""   (str:strs) = "":str:strs
    f str' (str:strs) = (str' ++ " " ++ str) : strs 

-- Debugging

getInput :: IO [String]
getInput = separatePassports <$> readFile "input"

test = sortFields "pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980 hcl:#623a2f"
