import Data.Vector (Vector, (!?))
import qualified Data.Vector as V

data Space = Open | Tree deriving (Show, Eq)

part1 :: [[Space]] -> Int
part1 spacess = undefined 

part1' :: Vector (Vector Space) -> Int
part1' vspacess = checkSlope vspacess (3,1)

-- Part 2

part2' :: Vector (Vector Space) -> Int
part2' vss = product $ map (checkSlope vss) [(1,1), (3,1), (5,1), (7,1), (1,2)]

-- Workhorse

checkSlope :: Vector (Vector Space) -> (Int,Int) -> Int
checkSlope vspacess (dx,dy) = sum $ fmap fromEnum $ V.unfoldr f (dx,dy)
  where
    f :: (Int,Int) -> Maybe (Bool, (Int,Int))
    f (x,y) = case index2DWrapped vspacess x y of
      Just Tree -> Just (True,  nextXY)
      Just Open -> Just (False, nextXY)
      Nothing   -> Nothing
      where
        nextXY = (x+dx,y+dy)

index2DWrapped :: Vector (Vector a) -> Int -> Int -> Maybe a
index2DWrapped vss x y = vss !? y >>= (`indexWrapped` x)

indexWrapped :: Vector a -> Int -> Maybe a
indexWrapped vs i = vs !? (i `mod` length vs)

make2DVector :: [[a]] -> Vector (Vector a)
make2DVector = V.fromList . map V.fromList

-- Main

main :: IO ()
main = do
  contents <- readFile "input" --"orion-input.txt"
  let spaceGrid = map (map openOrTree) $ lines contents
  print (part1 spaceGrid)

main' :: IO ()
main' = do
  x <- getInput
  print (part1' x)
  print (part2' x)

getInput :: IO (Vector (Vector Space))
getInput = do
  contents <- readFile "orion-input.txt"
  pure $ make2DVector $ map (map openOrTree) $ lines contents

openOrTree :: Char -> Space
openOrTree '#' = Tree
openOrTree '.' = Open
openOrTree  _  = error "Bad input file"