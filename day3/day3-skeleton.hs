data Space = Open | Tree deriving (Show, Eq)

part1 :: [[Space]] -> Int
part1 = error "Part 1 incomplete" 

part2 :: [[Space]] -> Int
part2 = error "Part 1 incomplete" 

main :: IO ()
main = do
  contents <- readFile "input"
  let spaceGrid = map (map openOrTree) $ lines contents
  print (part1 spaceGrid)
  print (part2 spaceGrid)

openOrTree :: Char -> Space
openOrTree '#' = Tree
openOrTree '.' = Open
openOrTree  _  = error "Bad input file"
