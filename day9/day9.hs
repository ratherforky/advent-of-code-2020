import Data.Vector (Vector)
import qualified Data.Vector as V
import Data.Maybe

valid :: Vector Int -> Int -> Int -> Bool
valid xs i nextI = elem (xs V.! nextI)
                 . fmap (uncurry (+))
                 . V.filter (\(x,y) -> x /= y)
                 $ vecCart window window
  where
    window = V.slice i 25 xs

vecCart :: Vector Int -> Vector Int -> Vector (Int,Int)
vecCart xs ys = do
  x <- xs
  y <- ys
  pure (x,y)

part1 :: Vector Int -> [Int]
part1 xs = map (\(i,i') -> xs V.! i')
         . filter (not . uncurry (valid xs)) 
         $ zip [0..] [25..V.length xs - 1] -- [(Start of window, number to check)]

sumRange :: Vector Int -> (Int, Int) -> Int
sumRange vec (start, end) = sum $ V.slice start (end - start) vec

minInRange :: Vector Int -> (Int, Int) -> Int
minInRange vec (start, end) = V.minimum $ V.slice start (end - start) vec

maxInRange :: Vector Int -> (Int, Int) -> Int
maxInRange vec (start, end) = V.maximum $ V.slice start (end - start) vec

part2 :: Vector Int -> Int -> [Int]
part2 vec p1Num = map (\x -> minInRange vec x + maxInRange vec x)
                . catMaybes
                . map findMonotonic
                $ [ [ (start, end) | end <- [start + 1..V.length vec]] | start <- [0..V.length vec - 1]]
  where
    findMonotonic :: [(Int, Int)] -> Maybe (Int, Int)
    findMonotonic []     = Nothing
    findMonotonic (x:xs)
      | summed < p1Num  = findMonotonic xs
      | summed == p1Num = Just x
      | summed > p1Num  = Nothing
      where
        summed = sumRange vec x

main :: IO ()
main = do
  contents <- readFile "input"
  let intLines = map read $ lines contents :: [Int]
      vecInts  = V.fromList intLines
      p1Num = head $ part1 vecInts
  print p1Num
  print (part2 vecInts p1Num)