import Data.Matrix (Matrix)
import qualified Data.Matrix as M
import Data.Maybe
import Data.List

data Cell = Floor | Empty | Occupied deriving (Eq, Show)

part1 :: Matrix Cell -> Int
part1 = count isOccupied
      . M.toList 
      . converge stepSeats 
-- part1 = converge stepSeats

converge :: Eq a => (a -> a) -> a -> a
converge f k = go $ iterate f k
  where
    go (x:y:ys)
      | x == y    = x
      | otherwise = go (y:ys)

stepSeats :: Matrix Cell -> Matrix Cell
stepSeats mc = M.imap (stepCell mc) mc

stepCell :: Matrix Cell -> (Int,Int) -> Cell -> Cell
stepCell mc coords c = case (c, numOccupied) of
  (Empty, 0)    -> Occupied
  (Occupied, n) -> if n >= 4 then Empty else Occupied
  (cell, _)     -> cell
  where
    numOccupied = count (isOccupied . (mc M.!)) $ surroundingCellCoords coords (M.dim mc)

isOccupied :: Cell -> Bool
isOccupied Occupied = True
isOccupied _        = False

count :: (a -> Bool) -> [a] -> Int
count p = length . filter p

surroundingCellCoords :: (Int, Int) -> (Int,Int) -> [(Int,Int)]
surroundingCellCoords (y,x) (maxY,maxX) 
  = [ (b,a) | b <- [clampY (y-1)..clampY (y+1)]
            , a <- [clampX (x-1)..clampX (x+1)]
            , not (b == y && a == x)
            ]
  where
    clampX = clamp0 (maxX - 1)
    clampY = clamp0 (maxY - 1)

clamp0 :: Int -> Int -> Int
clamp0 maxVal val = max 0 (min maxVal val)

-- Part 2

part2 :: Matrix Cell -> Int
part2 = count isOccupied
      . M.toList 
      . converge stepSeats'

stepSeats' :: Matrix Cell -> Matrix Cell
stepSeats' mc = M.imap (stepCell' mc) mc

stepCell' :: Matrix Cell -> (Int,Int) -> Cell -> Cell
stepCell' mc coords c = case (c, numOccupied mc coords) of
  (Empty, 0)    -> Occupied
  (Occupied, n) -> if n >= 5 then Empty else Occupied
  (cell, _)     -> cell

numOccupied :: Matrix Cell -> (Int,Int) -> Int
numOccupied mc coords 
  = count isOccupied
  . catMaybes
  . map (fmap (mc M.!) . find (notFloor . (mc M.!)))
  $ sightlines coords (M.dim mc)

notFloor :: Cell -> Bool
notFloor Floor = False
notFloor _     = True

sightlines :: (Int, Int) -> (Int,Int) -> [[(Int,Int)]]
sightlines (y,x) (maxY,maxX) 
  = [ [(b,x) | b <- [clampY (y-1), y-2..0], b /= y ]
    , [(b,x) | b <- [clampY (y+1)..maxY-1], b /= y ]
    , [(y,a) | a <- [clampX (x-1), x-2..0], a /= x ]
    , [(y,a) | a <- [clampX (x+1)..maxX-1], a /= x ]
    -- Diagonals
    , zip [y-1, y-2..0] [x-1, x-2..0]
    , zip [y+1..maxY-1] [x-1, x-2..0]
    , zip [y-1, y-2..0] [x+1..maxX-1]
    , zip [y+1..maxY-1] [x+1..maxX-1]
    ]
  where
    clampX = clamp0 (maxX - 1)
    clampY = clamp0 (maxY - 1)



-- Main

main :: IO ()
main = do
  contentLines <- fmap lines $ readFile "input"
  let cellMatrix = M.fromLists $ map (map parseCell) contentLines
  print $ part1 cellMatrix
  print $ part2 cellMatrix

parseCell :: Char -> Cell
parseCell '.' = Floor
parseCell 'L' = Empty
parseCell '#' = Occupied

getTest :: IO (Matrix Cell)
getTest = getInputMatrix "input-short"

getInputMatrix :: FilePath -> IO (Matrix Cell)
getInputMatrix filePath = M.fromLists 
                        . map (map parseCell) 
                        . lines
                      <$> readFile filePath
