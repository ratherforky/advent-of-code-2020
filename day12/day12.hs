import Forky.Parsing
import Control.Applicative
import Data.List

data Instr = Move CardinalDir Int
           | Turn RelativeDir Int
           | F Int
           deriving (Eq, Show)

data CardinalDir = N | E | S | W deriving (Eq, Show, Enum)
data RelativeDir = L | R deriving (Eq, Show)

data ShipShape = Ship
  { x   :: Int
  , y   :: Int
  , dir :: CardinalDir
  } deriving (Eq, Show)

part1 :: [Instr] -> Int
part1 = (\(Ship x y _) -> abs x + abs y) . foldl' f k
-- part1 = scanl' f k
  where
    k = Ship 0 0 E

    f :: ShipShape -> Instr -> ShipShape
    f s i = case i of
      Move dir dist   -> move dir dist s
      Turn dir degree -> turn dir degree s
      F dist          -> move (dir s) dist s
      
move :: CardinalDir -> Int -> ShipShape -> ShipShape
move direction dist ship = case direction of
  N -> ship { y = y ship + dist }
  E -> ship { x = x ship + dist }
  S -> ship { y = y ship - dist }
  W -> ship { x = x ship - dist }

turn :: RelativeDir -> Int -> ShipShape -> ShipShape
turn direction deg ship 
  = ship { dir = turn' direction deg (dir ship) }

turn' :: RelativeDir -> Int -> CardinalDir  -> CardinalDir
turn' rel deg card = toEnum
            . (`mod` 4)
            . op (fromEnum card)
            $ deg `div` 90
  where
    op = case rel of
      L -> (-)
      R -> (+)

-- Part 2 

data ShipWay = ShipW
  { shipPos :: (Int, Int)
  , wayPos  :: (Int, Int)
  } deriving (Eq, Show)

part2 :: [Instr] -> Int
part2 = (\(ShipW (x, y) _) -> abs x + abs y) . foldl' f k
-- part2 = scanl' f k
  where
    k = ShipW (0,0) (10,1)

    f :: ShipWay -> Instr -> ShipWay
    f s i = case i of
      Move dir dist   -> moveWay dir dist s
      Turn dir degree -> rotateShipWay dir degree s
      F dist          -> goTowardsWay dist s

moveWay :: CardinalDir -> Int -> ShipWay -> ShipWay
moveWay direction dist ship = case direction of
  N -> ship { wayPos = (x,y+dist) }
  E -> ship { wayPos = (x+dist,y) }
  S -> ship { wayPos = (x,y-dist) }
  W -> ship { wayPos = (x-dist,y) }
  where
    (x,y) = wayPos ship

goTowardsWay :: Int -> ShipWay -> ShipWay
goTowardsWay n (ShipW (x,y) (dx,dy)) = ShipW (x + dx * n, y + dy * n) (dx,dy)

rotateShipWay :: RelativeDir -> Int -> ShipWay -> ShipWay
rotateShipWay rel deg s = s { wayPos = rotateWay rel deg (wayPos s) }

rotateWay :: RelativeDir -> Int -> (Int, Int) -> (Int, Int)
rotateWay rel deg (x,y) = case rel of
  L -> case deg of
        90  -> (-y,x)
        180 -> (-x,-y)
        270 -> (y,-x)
  R -> case deg of
        90  -> (y,-x)
        180 -> (-x,-y)
        270 -> (-y,x)

-- Main

main :: IO ()
main = do
  instrs <- getInput
  print $ part1 instrs
  print $ part2 instrs


getInput :: IO [Instr]
getInput = getInputFrom "input"

getInputFrom :: FilePath -> IO [Instr]
getInputFrom filep = map (parseUnsafe instr)
                   . lines 
                 <$> readFile filep

-- Parsing

instr :: Parser Instr
instr = Move <$> cardinal <*> number
    <|> Turn <$> relative <*> number
    <|> F    <$  char 'F' <*> number

cardinal :: Parser CardinalDir
cardinal = choice
  [ N <$ char 'N'
  , E <$ char 'E'
  , S <$ char 'S'
  , W <$ char 'W'
  ]

relative :: Parser RelativeDir
relative = choice
  [ L <$ char 'L'
  , R <$ char 'R'
  ]